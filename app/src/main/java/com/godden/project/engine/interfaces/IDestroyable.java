package com.godden.project.engine.interfaces;

/**
 * An interface that provides a destroyable method for implementation.
 * All classes that may feature destroyable properties should implement this interface.
 * @author Joshua Godden
 * @version 1.0
 */
public interface IDestroyable {

    /**
     * Destroys the class
     */
    void destroy();
}
