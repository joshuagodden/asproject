package com.godden.project.engine;

import android.graphics.Canvas;

import com.godden.project.engine.interfaces.IDestroyable;

import java.util.ArrayList;

/**
 * A class which can handle multiple FxBasic objects for rendering and updating.
 * @author Joshua Godden
 * @version 1.0
 */
public class FxGroup extends FxBasic implements IDestroyable {

    /**
     * Short and quick member counter
     */
    protected byte membersCount;

    /**
     * The list of members in the group
     */
    protected ArrayList<FxBasic> members;

    /**
     * Creates and instantiates a new FxGroup
     */
    public FxGroup() {
        membersCount    = 0;
        members         = new ArrayList<>();
    }

    /**
     * Adds a new child to the member list
     * Returns if child already exists
     * Returns if the members count exceeds Byte.MAX_VALUE
     * @param child The child to be added to the list
     */
    public void add(FxBasic child) {
        if (membersCount == Byte.MAX_VALUE) return;
        if (members.contains(child))        return;
        members.add(child);
        membersCount++;
    }

    /**
     * Removes a child from the member list.
     * Returns if child does not exist
     * @param child The child to be removed from the list
     */
    public void remove(FxBasic child) {
        if (!members.contains(child)) return;
        members.remove(child);
        membersCount--;
    }

    /**
     * Gets the total size of the group
     * @return  A byte of total member count
     */
    public byte getSize() {
        return membersCount;
    }

    /**
     * Gets a specified element
     * @param index The index position
     * @return  A reference to the FxBasic in the group
     */
    public FxBasic get(int index) {
        return members.get(index);
    }

    @Override
    public void update() {
        for (byte i = 0; i < membersCount; i++) {
            FxBasic member = members.get(i);
            if (member.active) {
                member.update();
            }
        }
    }

    @Override
    public void draw(Canvas canvas) {
        for (byte i = 0; i < membersCount; i++) {
            FxBasic member = members.get(i);
            if (member.visible) {
                member.draw(canvas);
            }
        }
    }

    @Override
    public void destroy() {
        for (byte i = 0; i < membersCount; i++) {
            members.get(i).destroy();
        }
        members.clear();
    }
}
