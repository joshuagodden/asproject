package com.godden.project.engine;

import android.graphics.Canvas;
import android.util.Log;

/**
 * A class that handles the game loop of the engine. This class extends the Android thread and is required for the loop to run off the main UI Thread.
 * This class is actually based from an online resource and source and the original base functions/algorithms come from the following:
 * @see "http://www.javacodegeeks.com/2011/07/android-game-development-game-loop.html"
 * @author Joshua Godden
 * @version 1.0
 */
public class FxThread extends Thread {

    /**
     * A checker to see if the Thread is actually running
     */
    protected boolean isRunning;

    /**
     * A reference to the rendering view for FxEngine
     */
    protected FxView view;

    /**
     * The MAX frames per second this engine runs at
     */
    protected final int MAX_FPS             = 50;

    /**
     * The maximum amount of frame skips that can be performed
     */
    protected final int MAX_FRAME_SKIPS     = 5;

    /**
     * The total period for each frame
     */
    protected final int FRAME_PERIOD        = 1000 / MAX_FPS;

    /**
     * Creates and instantiates a new Thread
     * @param view  A reference to the renderer for FxEngine
     */
    public FxThread(FxView view) {
        super();
        this.isRunning  = false;
        this.view       = view;
    }

    /**
     * Tells the thread to start
     */
    public void setStart() {
        isRunning= true;
        start();
    }

    /**
     * Tells the thread to stop
     */
    public void setStop() {
        isRunning = false;
        boolean retry = true;
        while (retry) {
            try {
                join();
                retry = false;
            }
            catch (InterruptedException e) {
                //- failing to stop
            }
        }
    }

    @Override
    public void run() {
        Canvas canvas;
        Log.i(FxGlobal.LOG_DEBUG, "Game Loop Started");

        long beginTime      = 0;
        long timeDiff       = 0;
        int sleepTime       = 0;
        int framesSkipped   = 0;

        while (isRunning) {
            canvas = null;
            try {
                canvas = view.getHolder().lockCanvas();
                synchronized (view.getHolder()) {
                    beginTime = System.currentTimeMillis();
                    framesSkipped = 0;
                    view.onUpdate();
                    view.onDraw(canvas);
                    timeDiff    = System.currentTimeMillis() - beginTime;
                    sleepTime   = (int)(FRAME_PERIOD - timeDiff);

                    if (sleepTime > 0) {
                        try {
                            Thread.sleep(sleepTime);
                        }
                        catch (InterruptedException e) {
                            Log.e(FxGlobal.LOG_ERROR, "Couldn't make Thread sleep");
                        }
                    }

                    while (sleepTime < 0 && framesSkipped < MAX_FRAME_SKIPS) {
                        view.onUpdate();
                        sleepTime += FRAME_PERIOD;
                        framesSkipped++;
                    }
                }
            }
            finally {
                if (canvas != null) {
                    view.getHolder().unlockCanvasAndPost(canvas);
                }
            }
        }
    }
}
