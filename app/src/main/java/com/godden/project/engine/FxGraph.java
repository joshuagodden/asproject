package com.godden.project.engine;

import android.os.AsyncTask;
import android.util.Log;

import com.godden.project.engine.data.FxPoint;
import com.godden.project.engine.data.FxTile;
import com.godden.project.engine.interfaces.IDestroyable;
import com.godden.project.engine.interfaces.IPathFinder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 * A class that creates and handles the navigation graph along with A* path-finding for agents.
 * @author Joshua Godden
 * @version 1.0
 */
public class FxGraph implements IDestroyable {

    /**
     * The Navigation Graph
     */
    protected FxTile[][] navGraph;

    /**
     * How many tiles in width across the screen
     */
    protected byte screenWidthTiles;

    /**
     * How many tiles in height across the screen
     */
    protected byte screenHeightTiles;

    /**
     * A reference to the current task which is being used for Pathfinding
     */
    protected FxGraphTask currentTask;

    /**
     * Creates and instantiates a new Graph based system
     */
    public FxGraph() {
        screenWidthTiles    = 0;
        screenHeightTiles   = 0;
        currentTask         = null;
    }

    /**
     * Finds the path for a path finder
     * @param pathFinder    The Pathfinder to get a retrieved path
     */
    public void findPath(IPathFinder pathFinder) {

        //- check the task is not null and check its finished
        if (currentTask != null) {
            if (currentTask.getStatus() != AsyncTask.Status.FINISHED) {
                Log.i(FxGlobal.LOG_ERROR, "Current Async Task has not finished, cannot start a new one!!");
                return;
            }
        }

        //- create a new task
        currentTask = new FxGraphTask();
        currentTask.execute(pathFinder);
    }

    /**
     * Loads a map into the Graph system
     * @param data  The data to load
     */
    public void load(int[][] data) {
        if (data.length == 0) {
            Log.e(FxGlobal.LOG_PATH, "Cannot load map due to empty int array data");
            return;
        }

        //- fill the array with new data
        navGraph = new FxTile[data.length][data[0].length];
        for (int x = 0; x < data.length; x++) {
            for (int y = 0; y < data[x].length; y++) {
                boolean walkable = true;
                if (data[x][y] != 0) {
                    walkable = false;
                }
                navGraph[x][y] = new FxTile((short)data[x][y], (byte)y, (byte)x, walkable);
            }
        }

        //- set width and height of tiles
        this.screenWidthTiles   = (byte)data[0].length;
        this.screenHeightTiles  = (byte)data.length;
    }

    /**
     * Gets an ArrayList of free points available
     * @return  An ArrayList of all available points
     */
    public ArrayList<FxPoint> getFreePoints() {
        ArrayList<FxPoint> points = new ArrayList<>();
        for (int x = 0; x < navGraph.length; x++) {
            for (int y = 0; y < navGraph[x].length; y++) {
                FxTile tile = navGraph[x][y];
                if (tile.walkable) {
                    points.add(new FxPoint(tile.x, tile.y));
                }
            }
        }
        return points;
    }

    /**
     * Gets an ArrayList of free points available
     * @param end   The ending position of where you want to search
     * @return  An ArrayList of all available points
     */
    public ArrayList<FxPoint> getFreePoints(int end) {
        ArrayList<FxPoint> points = new ArrayList<>();
        for (int x = 0; x < end; x++) {
            for (int y = 0; y < navGraph[x].length; y++) {
                FxTile tile = navGraph[x][y];
                if (tile.walkable) {
                    points.add(new FxPoint(tile.x, tile.y));
                }
            }
        }
        return points;
    }

    /**
     * Gets the path from a working A* algorithm
     * @param start The starting position
     * @param end   The ending position
     * @return  Returns an ArrayList of the path points. Will return an empty ArrayList if the path cannot be completed
     */
    protected ArrayList<FxPoint> getPath(FxPoint start, FxPoint end) {

        //- tiles to represent, start, end and current. Keep a cloned map for working costs
        FxTile startTile                = navGraph[start.y][start.x];
        FxTile endTile                  = navGraph[end.y][end.x];
        FxTile currentTile              = null;
        FxTile[][] clonedMap            = navGraph;

        //- store our lists of open, closed and adjacent tiles (for memory usage, re-use them)
        ArrayList<FxTile> openList      = new ArrayList<>();
        ArrayList<FxTile> closedList    = new ArrayList<>();
        ArrayList<FxTile> adjacentList  = new ArrayList<>();

        //- store the path which will be return
        ArrayList<FxPoint> path         = new ArrayList<>();

        //== A* ALGORITHM =========================================================
        //- check early if graph is not empty
        if (navGraph.length == 0) {
            return path;
        }


        //- check to see if the start tile and end tile are walkable
        boolean search = true;
        if (!startTile.walkable) {
            return path;
        }
        if (!endTile.walkable) {
            return path;
        }

        //- search can begin
        if (search)
        {
            //- add to the open list first
            openList.add(startTile);

            //- loop until our openlist is empty or tile is found
            while (openList.size() != 0)
            {
                currentTile = getLowestTileTotal(openList, clonedMap);

                if (currentTile.equals(endTile)) {
                    Log.i(FxGlobal.LOG_PATH, "End tile successfully found");
                    break;
                }
                else
                {
                    //- remove from open, add to closed
                    openList.remove(currentTile);
                    closedList.add(currentTile);

                    //- go through adjacent tiles
                    adjacentList.clear();
                    adjacentList = getAdjacentTiles(currentTile);

                    for (Iterator<FxTile> i = adjacentList.iterator(); i.hasNext();) {
                        FxTile at = i.next();
                        if (!openList.contains(at)) {
                            if (!closedList.contains(at)) {
                                openList.add(at);
                                FxTile tile = clonedMap[at.y][at.x];
                                tile.cost       = (byte)(clonedMap[currentTile.y][currentTile.x].cost + 1);
                                tile.heuristic  = (byte)calculateHeuristicManhattan(at, endTile);
                                tile.total      = (byte)(tile.cost + tile.heuristic);
                            }
                        }
                    }
                }
            }
        }
        //======================================================================

        //== GET PATH ==========================================================
        boolean startFound  = false;
        FxTile searchTile   = endTile;
        path.add(new FxPoint(endTile.x, endTile.y));

        //- go through and see if we can find the start
        while (!startFound) {
            adjacentList.clear();
            adjacentList = getAdjacentTiles(searchTile);

            //- iterate through adjacent tiles
            for (Iterator<FxTile> n = adjacentList.iterator(); n.hasNext();) {
                FxTile na = n.next();

                //- check if its the start tile
                if (na.equals(startTile)) {
                    Log.i(FxGlobal.LOG_PATH, "StartTile was found in the list");
                    startFound = true;
                }

                //- check if it exists in the closed or open list
                if (closedList.contains(na) || openList.contains(na)) {
                    if (clonedMap[na.y][na.x].cost <= navGraph[searchTile.y][searchTile.x].cost
                            && clonedMap[na.y][na.x].cost > 0) {
                        searchTile = new FxTile(na.index, na.x, na.y, na.walkable);
                        path.add(new FxPoint(na.x, na.y));
                        break;
                    }
                }
            }
        }
        //======================================================================
        Collections.reverse(path);
        return path;
    }

    /**
     * Gets the lowest total scoring tile within a given openlist
     * @param openList  The open list to check
     * @return  Returns a tile with the lowest total
     */
    protected FxTile getLowestTileTotal(ArrayList<FxTile> openList, FxTile[][] map) {

        //- find the short and tile
        short lt        = Short.MAX_VALUE;
        FxTile tile     = null;

        //- go through open list
        for (Iterator<FxTile> p = openList.iterator(); p.hasNext();) {
            FxTile t = p.next();
            if (map[t.y][t.x].total <= lt) {
                lt      = map[t.y][t.x].total;
                tile    = new FxTile(t.index, t.x, t.y, t.walkable);
            }
        }

        return tile;
    }

    /**
     * Gets the adjacent tiles of a given tile
     * @param tile  The tile to be checked for its adjacent tiles
     * @return  Returns a list of available adjacent tiles
     */
    protected ArrayList<FxTile> getAdjacentTiles(FxTile tile) {
        ArrayList<FxTile> adjacentTiles = new ArrayList<>();
        FxTile at = null;

        //- below
        at = new FxTile(tile.index, tile.x, (byte)(tile.y + 1), tile.walkable);
        if (at.y < screenHeightTiles
                && navGraph[at.y][at.x].walkable) {
            adjacentTiles.add(at);
        }

        //- above
        at = new FxTile(tile.index, tile.x, (byte)(tile.y - 1), tile.walkable);
        if (at.y >= 0
                && navGraph[at.y][at.x].walkable) {
            adjacentTiles.add(at);
        }

        //- right
        at = new FxTile(tile.index, (byte)(tile.x + 1), tile.y, tile.walkable);
        if (at.x < screenWidthTiles
                && navGraph[at.y][at.x].walkable) {
            adjacentTiles.add(at);
        }

        //- left
        at = new FxTile(tile.index, (byte)(tile.x - 1), tile.y, tile.walkable);
        if (at.x >= 0
                && navGraph[at.y][at.x].walkable) {
            adjacentTiles.add(at);
        }

        return adjacentTiles;
    }

    /**
     * Calculates the estimate through the Manhattan method
     */
    protected int calculateHeuristicManhattan(FxTile adjacentTile, FxTile endTile) {
        int d1 = Math.abs(endTile.x - adjacentTile.x);
        int d2 = Math.abs(endTile.y - adjacentTile.y);
        return d1 + d2;
    }

    /**
     * Gets the total tiles of the screen width
     * @return  The total amount of tiles
     */
    public byte getScreenWidthTiles() {
        return screenWidthTiles;
    }

    /**
     * Gets the total tiles of the screen height
     * @return The total amount of tiles
     */
    public byte getScreenHeightTiles() {
        return screenHeightTiles;
    }

    @Override
    public void destroy() {
        currentTask.cancel(true);
        currentTask = null;
    }

    /**
     * Handles the path finding task asynchronously for working out the A* algorithm avoiding the UI-Thread
     */
    public final class FxGraphTask extends AsyncTask<IPathFinder, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(IPathFinder... params) {
            IPathFinder finder = params[0];
            finder.onMapComplete(getPath(finder.getStartPosition(),
                    finder.getEndPosition()));
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
        }
    }
}
