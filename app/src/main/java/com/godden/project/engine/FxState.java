package com.godden.project.engine;

import android.view.MotionEvent;

/**
 * A class that is used to handle state in FxGame. This class has to be extended if you want to make your own custom states.
 * @author Joshua Godden
 * @version 1.0
 */
public abstract class FxState extends FxGroup {

    /**
     * Creates and instantiates a new state
     */
    public FxState() {
        super();
    }

    /**
     * When the state is starting it is technically being created. This method is where all new variables should be created and added to the game state
     */
    public abstract void create();

    /**
     * When the game has been touched it will send an event through. This method takes the event that is passed from game.
     * @param event The MotionEvent that is passed through from FxGame.
     */
    public abstract void touch(MotionEvent event);
}
