package com.godden.project.engine;

import android.content.res.AssetManager;

/**
 * Static class where any globals may be required.
 * @author Joshua Godden
 * @version 1.0
 */
public final class FxGlobal {

    /**
     * Use for logging trace statements with level type Error. Useful for Logcat.
     */
    public static final String LOG_ERROR    = "Project_Error";

    /**
     * Use for logging trace statements with level type Debug. Useful for Logcat.
     */
    public static final String LOG_DEBUG    = "Project_Debug";

    /**
     * Use for logging trace statements with level type Pathfinding. Useful for Logcat.
     */
    public static final String LOG_PATH     = "Project_Path";

    /**
     * A reference to the general width of tiles of the Navigation Graph. Useful for real screen world coordinates.
     */
    public static final byte TILE_WIDTH      = 64;

    /**
     * A reference to the general height of tiles of the Navigation Graph. Useful for real screen world coordinates.
     */
    public static final byte TILE_HEIGHT     = 64;

    /**
     * A reference to the offset on the X axis the tiles are from the top left corner. Useful for real screen world coordinates.
     */
    public static final byte TILE_OFFSET_X   = 15;

    /**
     * A reference to the offset on the Y axis the tiles are from the top left corner. Useful for real screen world coordinates.
     */
    public static final byte TILE_OFFSET_Y   = 31;

    /**
     * A reference to the actual game being run.
     */
    public static FxGame        game;

    /**
     * A reference to the rendering view Android is using to run FxEngine.
     */
    public static FxView        view;

    /**
     * A reference to the AssetManager the Android application is currently using. Needed for loading FxEngine graphics.
     */
    public static AssetManager  assetManager;
}
