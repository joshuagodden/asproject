package com.godden.project.engine;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.godden.project.game.GameState;

/**
 * A class which extends SurfaceView for the rendering of FxEngine.
 * @author Joshua Godden
 * @version 1.0
 */
public class FxView extends SurfaceView {

    /**
     * An instance of the FxThread which is used for running FxEngine
     */
    protected FxThread thread;

    /**
     * An instance of the FxEngine
     */
    protected FxGame game;


    /**
     * Sets up the holder
     */
    protected void setupHolder() {

        //- append thread
        thread  = new FxThread(this);

        //- add the game
        game    = new FxGame();

        //- setup holder
        getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                thread.setStart();
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                game.changeState(new GameState());
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                thread.setStop();
                game.destroy();
            }
        });
    }

    @Override
    public void onDraw(Canvas canvas) {
        canvas.drawColor(Color.BLACK);
        game.draw(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        game.touch(event);
        return super.onTouchEvent(event);
    }

    /**
     * Updates the instance, along with the actual game
     */
    public void onUpdate() {
        game.update();
    }

    public FxView(Context context) {
        super(context);
        setupHolder();
    }

    public FxView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupHolder();
    }

    public FxView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setupHolder();
    }
}
