package com.godden.project.engine.data;

/**
 * A class that represents a data format used for handling node positions in pathfinding
 * @author Joshua Godden
 * @version 1.0
*/
public final class FxPoint {

    /**
     * The X value
     */
    public byte x;

    /**
     * The Y value
     */
    public byte y;

    /**
     * Creates and instantiates a new FxPoint
     * @param x The X position
     * @param y The Y position
     */
    public FxPoint(int x, int y) {
        this.x = (byte)x;
        this.y = (byte)y;
    }

    /**
     * A Zero based Point
     * @return  A new FxPoint with 0,0
     */
    public static FxPoint Zero() {
        return new FxPoint((byte)0,(byte)0);
    }

    @Override
    public boolean equals(Object o) {
        FxPoint c = (FxPoint)o;
        if (c.x == x && c.y == y) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return Integer.toString(x) + "," + Integer.toString(y);
    }

}
