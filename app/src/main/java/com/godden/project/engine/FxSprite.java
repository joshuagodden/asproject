package com.godden.project.engine;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;

import java.io.IOException;

/**
 * A class that is used for drawing and animating sprite objects in the game. Extend this class for adding custom behaviours and drawing techniques.
 * @author Joshua Godden
 * @version 1.0
 */
public class FxSprite extends FxBasic {

    /**
     * A reference to the Bitmap data that is used for drawing.
     */
    protected Bitmap bitmap;

    /**
     * The filename of the Bitmap data to be loaded.
     */
    protected String filename;

    /**
     * A check for if the sprite is to be animated.
     */
    protected boolean animated;

    /**
     * Animation: The total number of columns on screen.
     */
    protected byte frameColumns;

    /**
     * Animation: The total number of rows on screen.
     */
    protected byte frameRows;

    /**
     * Animation: The current frame being displayed.
     */
    protected byte frameCount;

    /**
     * Creates and instantiates a new Sprite.
     * @param filename  The location of the file/Sprite to load
     * @param x         The x position
     * @param y         The y position
     */
    public FxSprite(String filename, int x, int y) {
        this.x          = (short)x;
        this.y          = (short)y;
        this.filename   = filename;
        this.animated   = false;
        load();
    }

    /**
     * Creates and instantiates a new Sprite.
     * @param filename      The location of the file/Sprite to load
     * @param x             The x position
     * @param y             The y position
     * @param frameColumns  The column count in the Sprite sheet
     * @param frameRows     The row count in the Sprite sheet
     * @param frameWidth    The width per Sprite/frame
     * @param frameHeight   The height per Sprite/frame
     */
    public FxSprite(String filename, int x, int y, int frameColumns, int frameRows, int frameWidth, int frameHeight) {
        this.filename       = filename;
        this.x              = (short)x;
        this.y              = (short)y;
        this.frameColumns   = (byte)frameColumns;
        this.frameRows      = (byte)frameRows;
        this.width          = (short)frameWidth;
        this.height         = (short)frameHeight;
        this.animated       = true;
        load();
    }

    /**
     * Loads the Bitmap data which will be used for drawing the object on screen.
     */
    protected void load() {
        try {
            bitmap  = BitmapFactory.decodeStream(FxGlobal.assetManager.open(filename));
            if (!animated) {
                width   = (short)bitmap.getWidth();
                height  = (short)bitmap.getHeight();
            }
        }
        catch (IOException exception) {
            Log.i(FxGlobal.LOG_ERROR, "Cannot load bitmap for Sprite");
        }
    }

    /**
     * Checks to see if the sprite has collided with another Sprite
     * @param x2    The sprite to check's X position
     * @param y2    The sprite to check's Y position
     * @return  Returns a boolean on whether the collision was true or not
     */
    public boolean collides(float x2, float y2) {
        return x2 > x && x2 < x + width && y2 > y && y2 < y + height;
    }

    @Override
    public void update() {
        if (animated) {
            frameCount = (byte)(++frameCount % frameColumns);
        }
    }

    @Override
    public void draw(Canvas canvas) {
        if (animated) {
            int srcX = frameCount * width;
            int srcY = 0 * height;
            Rect src = new Rect(srcX, srcY, srcX + width, srcY + height);
            Rect dst = new Rect(x, y, x + width, y + width);
            canvas.drawBitmap(bitmap, src, dst, null);
        }
        else {
            canvas.drawBitmap(bitmap, x, y, null);
        }
    }

    @Override
    public void destroy() {
        bitmap.recycle();
        bitmap = null;
    }
}
