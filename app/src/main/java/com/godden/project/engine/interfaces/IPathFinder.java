package com.godden.project.engine.interfaces;

import com.godden.project.engine.data.FxPoint;

import java.util.ArrayList;

/**
 * A class that is designed to become an Agent should implement this class to work correctly with the FxGraph class.
 * @author Joshua Godden
 * @version 1.0
 */
public interface IPathFinder {

    /**
     * When the shortest path has been found by FxGraph it will be sent to this function.
     * @param path  The shortest path tree found in the navigation grid
     */
    void onMapComplete(ArrayList<FxPoint> path);

    /**
     * Gets the starting position of this agent for the path-finding to start from.
     * @return  Returns a FxPoint which is the starting position of this agent.
     */
    FxPoint getStartPosition();

    /**
     * Gets the end position that this agent will be using for its destination.
     * @return  Returns a FxPoint which is the destination of this agent.
     */
    FxPoint getEndPosition();
}
