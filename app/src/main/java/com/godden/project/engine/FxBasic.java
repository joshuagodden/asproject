package com.godden.project.engine;

import android.graphics.Canvas;

import com.godden.project.engine.interfaces.IDestroyable;

/**
 * An abstract class that all classes related to FxEngine extend from. Any classes that are designed to be used by
 * FxEngine should extend this class.
 * @author Joshua Godden
 * @version 1.0
 */
public abstract class FxBasic implements IDestroyable {

    /**
     * The X position of the object
     */
    public short x        = 0;

    /**
     * The Y position of the object
     */
    public short y        = 0;

    /**
     * The width of the object
     */
    public short width    = 0;

    /**
     * The height of the object
     */
    public short height   = 0;

    /**
     * If the object is active it will be updated by the state
     */
    public boolean active   = true;

    /**
     * If the object is visible it will be rendered by the state
     */
    public boolean visible  = true;

    /**
     * Updates the object such as its behaviour and movement
     */
    public abstract void update();

    /**
     * Draws the object such as its sprite sheet etc.
     * @param canvas    The canvas object that is used by the game for drawing objects on screen
     */
    public abstract void draw(Canvas canvas);

    /**
     * Destroys the object. Use this for setting variables to null or destroying other objects.
     */
    public abstract void destroy();
}
