package com.godden.project.engine;

/**
 * A utility class for performing a variety of useful functions.
 * @author Joshua Godden
 * @version 1.0
 */
public final class FxUtil {

    /**
     * Converts a point X to a positional X
     * @param x The point X position
     * @return  Returns a calculated positional X
     */
    public static short convertPointToWorldX(byte x) {
        return (short)(FxGlobal.TILE_OFFSET_X + (x * FxGlobal.TILE_WIDTH));
    }

    /**
     * Converts a point Y to a positional Y
     * @param y The point Y position
     * @return  Returns a calculated positional Y
     */
    public static short convertPointToWorldY(byte y) {
        return (short)(FxGlobal.TILE_OFFSET_Y + (y * FxGlobal.TILE_HEIGHT));
    }

    /**
     * Gets the distance between two points
     * @param x1    The first object's X position
     * @param y1    The first object's Y position
     * @param x2    The second object's X position
     * @param y2    The second object's Y position
     * @return  Returns the distance between the two positions
     */
    public static short getDistance(short x1, short y1, short x2, short y2) {
        short mx = (short)(x1 - x2);
        short my = (short)(y1 - y2);
        return (short)Math.sqrt(mx * mx + my * my);
    }
}
