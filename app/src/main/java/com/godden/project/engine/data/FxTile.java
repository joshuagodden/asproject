package com.godden.project.engine.data;

/**
 * A class that represents a tile node on a navigation grid graph. All variables types are specifically chosen
 * to reduce the memory overhead costs and processing times when pathfinding is in use.
 * @author Joshua Godden
 * @version 1.0
 */
public final class FxTile {

    /**
     * The index correlating to the tile position in the navigation grid.
     */
    public short index;

    /**
     * The X position of the grid this tile is based
     */
    public byte x;

    /**
     * The Y position of the grid this tile is based
     */
    public byte y;

    /**
     * The cost of the tile
     */
    public byte cost;

    /**
     * The heuristic of the tile
     */
    public byte heuristic;

    /**
     * The total cost of the tile
     */
    public byte total;

    /**
     * If this tile is walkable
     */
    public boolean walkable;

    /**
     * Creates and instantiates a new Tile
     * @param index The index in the int[][]
     * @param x     The x position
     * @param y     The y position
     */
    public FxTile(short index, byte x, byte y, boolean walkable) {
        this.index      = index;
        this.walkable   = walkable;
        this.x          = x;
        this.y          = y;
    }

    @Override
    public String toString() {
        return "([" + x + "," + y + "] | I [" + index + "] | C [" + cost + "] | H [" + heuristic + "] | T [" + total + "] | W [" + walkable +"])";
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof FxTile) {
            FxTile t = (FxTile) o;
            return ((t.x == x && t.y == y)) ? true : false;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = index;
        result = 31 * result + x;
        result = 31 * result + y;
        return result;
    }
}
