package com.godden.project.engine;

import android.graphics.Canvas;
import android.view.MotionEvent;

import com.godden.project.engine.interfaces.IDestroyable;

/**
 * A class that represents a game for the FxEngine. Game handles all state changes, updates, drawing, touch events and destroying.
 * @author Joshua Godden
 * @version 1.0
 */
public class FxGame implements IDestroyable {

    /**
     * The current state that is being updated, drawn etc.
     */
    protected FxState state;

    /**
     * A reference to the previous state.
     */
    protected FxState initialState;

    /**
     * If a change state has been requested this boolean switches and any updating and drawing is paused.
     */
    protected boolean requestState;

    /**
     * Creates and instantiates a new Game
     */
    public FxGame() {
        FxGlobal.game   = this;
        requestState    = false;
    }

    /**
     * Updates the game
     */
    public void update() {
        if (!requestState) {
            if (state != null) {
                state.update();
            }
        }
    }

    /**
     * Draws the game
     * @param canvas    The canvas provided by FxView for drawing images in the game
     */
    public void draw(Canvas canvas) {
        if (!requestState) {
            if (state != null) {
                state.draw(canvas);
            }
        }
    }

    /**
     * Called when the game screen has been touched
     * @param event The MotionEvent that has been passed through when the screen is touched
     */
    public void touch(MotionEvent event) {
        if (!requestState) {
            if (state != null) {
                state.touch(event);
            }
        }
    }

    /**
     * Changes the current state in the game
     * @param newState  The state that the game should be changed too
     */
    public void changeState(FxState newState) {

        //- make initial state the original and update
        initialState = state;
        requestState = true;

        //- new state now
        if (state != null) {
            state.destroy();
        }
        state = newState;
        state.create();
        requestState = false;
    }

    @Override
    public void destroy() {
        //state.destroy();
        //initialState.destroy();
    }
}
