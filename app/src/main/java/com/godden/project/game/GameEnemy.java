package com.godden.project.game;

import com.godden.project.engine.FxSprite;
import com.godden.project.engine.FxUtil;
import com.godden.project.engine.data.FxPoint;
import com.godden.project.engine.interfaces.IPathFinder;

import java.util.ArrayList;

/**
 * A simple enemy working with the engine and path finding
 * @author Joshua Godden
 * @version 1.0
 */
public final class GameEnemy extends FxSprite implements IPathFinder {

    //- a* management
    protected ArrayList<FxPoint> path;
    protected FxPoint startPosition;
    protected FxPoint endPosition;

    //- movement processing variables
    protected short targetX;
    protected short targetY;
    protected boolean moving;
    protected boolean canMove;
    protected boolean hasPath;

    //- movement processing finals
    protected final short THRESHOLD = 8;
    protected final byte SPEED      = 10;

    /**
     * Creates and instantiates a new Enemy
     */
    public GameEnemy() {
        super("graphics/enemies_hdpi.png", 0, 0, 4, 1, 64, 64);
        this.moving     = false;
        this.hasPath    = false;
        this.canMove    = false;
    }

    @Override
    public void update() {
        super.update();
        updatePathMovement();
    }

    @Override
    public void destroy() {
        super.destroy();
        path.clear();
    }

    @Override
    public void onMapComplete(ArrayList<FxPoint> foundPath) {
        if (path != null) {
            path.clear();
        }
        this.path       = foundPath;
        this.canMove    = true;
        this.hasPath    = true;
        this.setTarget(path.get(0));
    }

    @Override
    public FxPoint getStartPosition() {
        return startPosition;
    }

    @Override
    public FxPoint getEndPosition() {
        return endPosition;
    }

    /**
     * Sets the start position for the enemy
     * @param start The start position
     */
    public void setStartPosition(FxPoint start) {
        this.x = FxUtil.convertPointToWorldX(start.x);
        this.y = FxUtil.convertPointToWorldY(start.y);
        startPosition = start;
    }

    /**
     * Sets the end position for the enemy
     * @param end   The end position
     */
    public void setEndPosition(FxPoint end) {
        endPosition = end;
    }

    /**
     * Updates the target being tracked by the enemy
     * @param target    The new target to track
     */
    private void setTarget(FxPoint target) {
        targetX = FxUtil.convertPointToWorldX(target.x);
        targetY = FxUtil.convertPointToWorldY(target.y);
    }

    /**
     * Updates the position of the enemy
     */
    private void updatePosition() {
        if (x > targetX) { x -= (1 * SPEED); }
        if (x < targetX) { x += (1 * SPEED); }
        if (y > targetY) { y -= (1 * SPEED); }
        if (y < targetY) { y += (1 * SPEED); }
    }

    /**
     * Updates the path movement
     */
    private void updatePathMovement() {
        if (hasPath) {
            if (!moving) {
                if (path.size() != 0) {
                    setTarget(path.get(0));
                    path.remove(0);
                    moving = true;
                } else {
                    canMove = false;
                }
            } else {
                short distance = FxUtil.getDistance(x, y, targetX, targetY);
                if (distance >= THRESHOLD + SPEED) {
                    updatePosition();
                } else {
                    moving = false;
                }
            }
        }
    }
}
