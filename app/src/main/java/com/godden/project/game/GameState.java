package com.godden.project.game;

import android.util.Log;
import android.view.MotionEvent;

import com.godden.project.engine.FxGlobal;
import com.godden.project.engine.FxGraph;
import com.godden.project.engine.FxGroup;
import com.godden.project.engine.FxSprite;
import com.godden.project.engine.FxState;
import com.godden.project.engine.data.FxPoint;

import java.util.ArrayList;
import java.util.Random;

/**
 * A demonstration state to show capabilities of FxEngine and Navigation Graph with A* pathfinding
 * @author Joshua Godden
 * @version 1.0
 */
public class GameState extends FxState {

    /**
     * An instance of the example enemy
     */
    private GameEnemy           _enemy;

    /**
     * An instance of enemy group
     */
    private FxGroup             _enemyGroup;

    /**
     * An instance of the example player
     */
    private GamePlayer          _player;

    /**
     * An instance of the Navigation Graph which will be used for pathfinding by the enemy
     */
    private FxGraph              _graph;

    /**
     * An instance of the background graphic
     */
    private FxSprite            _background;

    /**
     * A list of free points available on the FxGraph
     */
    private ArrayList<FxPoint>  _freePoints;

    /**
     * An instance of Random for just random positioning of enemies
     */
    private Random              _random;

    private boolean             _started = false;

    @Override
    public void create() {
        //- create and instantiate a navigation graph
        _graph = new FxGraph();
        _graph.load(new int[][]
               {{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                {1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1},
                {1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1},
                {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                {1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1},
                {1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1},
                {1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}}
        );

        //- instantiate the background, enemy and player
        _background     = new FxSprite("graphics/background_hdpi.png", 0, 0);
        _player         = new GamePlayer();
        _enemy          = new GameEnemy();

        //- add them all to the scene. Z-ordering applies here
        add(_background);
        add(_player);
        add(_enemy);

        //- get some free points and setup random
        _freePoints     = _graph.getFreePoints(5);
        _random         = new Random();
    }

    /**
     * Set the positions to start the game
     */
    private void setStart() {
        for (int i = 0; i < _enemyGroup.getSize(); i++) {
            Log.i(FxGlobal.LOG_DEBUG, "Enemy started");
            GameEnemy enemy = (GameEnemy)_enemyGroup.get(i);
            enemy.setStartPosition(_freePoints.get(_random.nextInt(_freePoints.size())));
            enemy.setEndPosition(_player.getPosition());
            _graph.findPath(enemy);
        }
    }

    @Override
    public void touch(MotionEvent event) {
        final FxPoint randomStart = _freePoints.get(_random.nextInt(_freePoints.size()));
        _enemy.setStartPosition(randomStart);
        _enemy.setEndPosition(_player.getPosition());
        _graph.findPath(_enemy);
    }

    @Override
    public void update() {
        super.update();
    }
}
