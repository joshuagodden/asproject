package com.godden.project.game;

import com.godden.project.engine.FxSprite;
import com.godden.project.engine.data.FxPoint;

/**
 * A class which handles the representation of the player
 * @author Joshua Godden
 * @version 1.0
 */
public class GamePlayer extends FxSprite {

    /**
     * The position of the player for FxPoint
     */
    private FxPoint _position;

    /**
     * Creates and instantiates a new Player
     */
    public GamePlayer() {
        super("graphics/player_hdpi.png", 365, 1055, 2, 1, 64, 64);
        _position = new FxPoint(5, 16);
    }

    public FxPoint getPosition() {
        return _position;
    }
}
