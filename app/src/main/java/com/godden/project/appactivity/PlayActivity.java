package com.godden.project.appactivity;

import android.app.Activity;
import android.os.Bundle;

import com.godden.asproject.myapplication.R;
import com.godden.project.engine.FxGlobal;

/**
 * PlayActivity is the main activity Android will load for this application.
 * @author Joshua Godden
 * @version 1.0
 */
public class PlayActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        //== setup some statics yo
        FxGlobal.assetManager   = getAssets();
    }
}
